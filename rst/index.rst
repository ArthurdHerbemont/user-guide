User's Guide
============

Please find the PDF version of the AIMMS User's Guide `here <https://documentation.aimms.com/_downloads/AIMMS_user.pdf>`__.

.. toctree::

   about-aimms/index
   preface/index
   introduction-to-aimms/index
   creating-and-managing-a-model/index
   creating-an-end-user-interface/index
   data-management/index
   miscellaneous/index

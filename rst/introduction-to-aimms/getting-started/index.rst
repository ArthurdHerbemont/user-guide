Getting Started
===============

To be written...

.. toctree::

   getting-started-with-aimms
   creating-a-new-project
   modeling-tools
   dockable-windows
   additional-files-related-to-an-aimms-project
   aimms-licensing

Procedures and Functions
========================

To be written...

.. toctree::

   creating-procedures-and-functions
   declaration-of-arguments-and-local-identifiers
   specifying-the-body
   syntax-checking,-compilation-and-execution
